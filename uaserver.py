#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import secrets
from uaclient import SmallSMILHandler
from xml.sax import make_parser


class EchoHandler(socketserver.DatagramRequestHandler):

    """Echo server class."""

    audiortp = []

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)

        texto = self.rfile.read().decode('utf-8')
        print(texto)
        metodo = texto.split(" ")[0]
        if metodo == 'INVITE':
            ip = texto.split('\r\n')[5].split(' ')[1]
            port = int(texto.split('\r\n')[8].split(' ')[1])
            LINE = 'SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing' \
                   '\r\n\r\nSIP/2.0 200 OK\r\n'
            sdp = "v=0\r\no=" + configuracion["account-username"] + ' ' + \
                  direccip + "\r\ns=papisesion\r\nt=0\r\nm=audio " \
                  + rtpaudio + " RTP"
            LINE += 'Content-Type: application/sdp\r\nContent-Lenght: ' \
                    + str(len(sdp))
            LINE += '\r\n\r\n' + sdp + '\r\n\r\n'
            self.wfile.write(bytes(LINE, 'utf-8'))
            self.audiortp.append(ip)
            self.audiortp.append(port)

        if metodo == 'ACK':
            BIT = secrets.randbelow(1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT,
                                  payload_type=14, ssrc=200002)
            audio = simplertp.RtpPayloadMp3(configuracion['audio-path'])
            simplertp.send_rtp_packet(RTP_header, audio,
                                      self.audiortp[0], self.audiortp[1])

        if metodo == 'BYE':
            self.audiortp = []
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')


if __name__ == "__main__":

    # Creamos servidor de eco y escuchamos

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    archivo = sys.argv[1]
    LOGS = 'logpath.txt'
    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit('FIle not found')
    configuracion = cHandler.get_tags()
    print(configuracion)

    if configuracion['uaserver-ip'] == ' ':
        direccip = '127.0.0.1'

    else:
        direccip = configuracion['uaserver-ip']

    if configuracion['regproxy-ip'] == ' ':
        proxyip = '127.0.0.1'

    else:
        proxyip = configuracion['regproxy-ip']

    puerto = configuracion['uaserver-puerto']
    usuario = configuracion['account-username']
    contrasena = configuracion['account-passwd']
    rtpaudio = configuracion['rtpaudio-puerto']
    proxyport = configuracion['regproxy-puerto']
    mp3 = 'cancion.mp3'
    serv = socketserver.UDPServer((direccip, int(puerto)), EchoHandler)

    print("Listening...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit("Finalizando el servidor")
