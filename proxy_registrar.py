#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import socket
import sys
import json
import time
import secrets
from uaclient import log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib


class proxyHandler(ContentHandler):

    def __init__(self):

        self.lista_etiquetas = {}
        self.etiqueta = ''
        self.dic = {'server': ['name', 'ip', 'puerto'],
                    'database': ['path', 'pswdpath'],
                    'log': ['']}

    def startElement(self, name, attrs):
        if name in self.dic:
            self.etiqueta = name
            for atributo in self.dic[name]:
                if name != 'log':
                    self.lista_etiquetas[name + '_'
                                         + atributo] = attrs.get(atributo, '')

    def endElement(self, name):
        self.etiqueta = ''

    def characters(self, content):
        if self.etiqueta == 'log':
            self.lista_etiquetas[self.etiqueta] = content

    def get_tags(self):
        return self.lista_etiquetas


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}
    diccpasswd = {}
    hexadecimal = {}

    def register2json(self):

        with open(client_db, 'w') as fichero_json:
            json.dump(self.dicc, fichero_json, indent=4)
            print("Usuarios registrados")

    def json2passwords(self):

        try:
            with open(client_db, "r") as jsonfile:
                self.dicc = json.load(jsonfile)
                print("Actualizamos página")

            with open(password_db, "r") as jsonfile:
                self.diccpasswd = json.load(jsonfile)
                print("Actualizamos página")

        except FileNotFoundError:
            pass

    def tiempoexpirado(self):
        tiempoactual = time.time() + 3600

        for email in self.dicc.copy():
            tiempocaduc = self.dicc[email]['Fecha'] \
                          + self.dicc[email]['Caducidad']
            if tiempoactual >= tiempocaduc:
                del self.dicc[email]

    def enviocliente(self, ip, port, linea):
        """Envio mensajes al uaclient."""
        self.wfile.write(bytes(linea, 'utf-8'))
        print('mandamos al cliente: ', linea)
        linea2 = linea.replace("\r\n", " ")
        log('Sent to ' + ip + ':' + str(port) + ': ' + linea2, LOGS)

    def envioserver(self, ip, port, mensaje):

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((ip, port))
            my_socket.send(bytes(mensaje, 'utf-8'))
            log('Sent to ' + ip + ':' + str(port) + ': ' + mensaje, LOGS)
            data = my_socket.recv(1024)
            log('Received from ' + ip + ':' + str(port) + ': '
                + data.decode('utf-8'), LOGS)
        return data.decode('utf-8')

    def hash(self, nonce, contrasena):
        digest = hashlib.sha224()
        digest.update(bytes(nonce + contrasena, 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def handle(self):

        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        linea = self.rfile.read()
        texto = linea.decode('utf-8')
        self.json2passwords()
        self.tiempoexpirado()
        metodo = texto.split(" ")[0]
        IP = self.client_address[0]
        PORT = self.client_address[1]

        if metodo in ["INVITE", "ACK", "BYE", "REGISTER"]:
            if texto.split(" ")[1].split(":")[0] != "sip":
                self.enviocliente(IP, PORT, 'SIP/2.0 400 Bad Request\r\n\r\n')

            else:

                if metodo == 'INVITE':
                    print(texto)
                    destino = texto.split(' ')[1].split(':')[1]
                    origen = texto.split('\r\n')[5].split(' ')[0].split('=')[1]
                    if destino in self.dicc and origen in self.dicc:
                        data = \
                            self.envioserver(self.dicc[destino]['IP'],
                                             self.dicc[destino]['PORT'], texto)
                        self.enviocliente(IP, PORT, data)
                    else:
                        self.enviocliente(IP, PORT,
                                          'SIP/2.0 404 User Not Found\r\n\r\n')

                elif metodo == 'ACK':
                    destino = texto.split(' ')[1].split(':')[1]
                    if destino in self.dicc:
                        data = \
                            self.envioserver(self.dicc[destino]['IP'],
                                             self.dicc[destino]['PORT'], texto)
                    else:
                        self.enviocliente(IP, PORT,
                                          'SIP/2.0 404 User Not Found\r\n\r\n')

                elif metodo == "BYE":
                    destino = texto.split(' ')[1].split(':')[1]
                    if destino in self.dicc:
                        data = \
                            self.envioserver(self.dicc[destino]['IP'],
                                             self.dicc[destino]['PORT'], texto)
                        self.enviocliente(IP, PORT, data)
                    else:
                        self.enviocliente(IP, PORT,
                                          'SIP/2.0 404 User Not Found\r\n\r\n')

                elif metodo == 'REGISTER':
                    print(texto)
                    email = texto.split(" ")[1].split(':')[1]
                    puerto = texto.split(" ")[1].split(':')[2]
                    caducidad = int(texto.split("\r\n")[1].split(":")[1])
                    tiempoactual = time.time() + 3600
                    if email in self.dicc:
                        if caducidad == 0:
                            del self.dicc[email]
                        elif caducidad != 0:
                            self.dicc[email]['IP'] = self.client_address[0]
                            self.dicc[email]['PORT'] = int(puerto)
                            self.dicc[email]['Fecha'] = tiempoactual
                            self.dicc[email]['Caducidad'] = caducidad
                        self.enviocliente(IP, PORT, 'SIP/2.0 200 OK\r\n\r\n')
                    elif email not in self.dicc:

                        if 'Authorization: Digest response' in texto:
                            nonceclient = texto.split('\r\n')[2].split('"')[1]
                            hash = self.hash(self.hexadecimal[email],
                                             self.diccpasswd[email])
                            if secrets.compare_digest(nonceclient, hash):
                                self.dicc[email] = \
                                    {'IP': self.client_address[0],
                                     'PORT': int(puerto),
                                     'Fecha': tiempoactual,
                                     'Caducidad': caducidad}
                                resp = 'SIP/2.0 200 OK\r\n\r\n'
                                print(resp)
                                self.enviocliente(IP, PORT, resp)

                        else:
                            self.hexadecimal[email] = secrets.token_hex(2)
                            resp = 'SIP/2.0 401 Unauthorized\r\n' + \
                                   'WWW Authenticate: Digest nonce="' \
                                   + self.hexadecimal[email] + '"\r\n\r\n'
                            self.wfile.write(bytes(resp, 'utf-8'))

            print(self.dicc)
            self.register2json()


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    parser = make_parser()
    cHandler = proxyHandler()
    parser.setContentHandler(cHandler)
    archivo = sys.argv[1]
    parser.parse(open(archivo))
    configuracion = cHandler.get_tags()
    PORT_server = int(configuracion['server_puerto'])
    IP_server = configuracion['server_ip']
    client_db = configuracion['database_path']
    password_db = configuracion['database_pswdpath']
    LOGS = configuracion['log']

    serv = \
        socketserver.UDPServer((IP_server, PORT_server), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
