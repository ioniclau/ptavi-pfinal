ptavi-pfinal
============

Ficheros para realizar la práctica final de PTAVI.

Asignatura y Grado:
Protocolos para la Transmisión de Audio y Vídeo en Internet
Grado en Ingeniería de Sistemas Audiovisuales y Multimedia

Departamento, Escuela y Universidad:
Grupo de Sistemas y Comunicaciones
Escuela Técnica Superior de Ingenieros de Telecomuncación
Universidad Rey Juan Carlos

He tenido un problema, y es el siguiente:
A la hora de clonar el repositorio, se me olvidó realizar el fork. Yo empecé la práctica la semana pasada (la del 25 de enero) e hice algunos commits, sin darme cuenta todavía de que no había hecho el fork. Me dí cuenta ayer, cuando quise hacer el push, me di cuenta del problema, y es que estaba haciendo los commits en otro repositorio, en el original de la práctica, donde hay que hacer el fork.
Entonces borré el repositorio de mi GitLab, hice el fork y el clone, y ahora sí subí los archivos manualmente a mi repositorio, con la opción de Upload File, por tanto lo subí uno a uno, todos los archivos terminados. 
El problema está en que perdí los commits hechos anteriormente, y no se refleja mi avance desde que empecé. 
Se lo he comentado al profesor Jesús, y me ha recomendado explicarlo aquí para que se tenga en cuenta.

Un saludo.
